package com.example.logcatactivitylifecycle

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.logcatactivitylifecycle.databinding.FragmentFirstBinding
import com.example.logcatactivitylifecycle.util.Constants.TAG

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate: ON CREATE VIEWMODEL")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView: ON CREATE VIEWMODEL CALLBACK")
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated: ")

        binding.buttonFirst.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        Log.d(TAG, "onViewStateRestored: ON VIEW STATE RESTORED VIEWMODEL")
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart: ON STRAT VIEW MODEL ")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: ONRESUME VIEW MODEL")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(TAG, "onSaveInstanceState: onSAVEDINSTANCESTATE VIEWMODEL")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause: ONPAUSE VIEWMODEL")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop: on STOP VIEWMODEL")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG, "onDestroyView:VIEWMODEL CALLBACK ")
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy: ONDESTROY VIEWMODEL")
    }
}
